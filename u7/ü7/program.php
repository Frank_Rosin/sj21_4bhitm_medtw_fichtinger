<?php
function autoload($className){
    if(file_exists("./{$className}.php")){
        require "./{$className}.php";
    } }

spl_autoload_register('autoload');

autoload("Car");
autoload("Car2");
autoload("SuperCar");
$vw = new Car();
$vw->drive();
$mbw = new Car();
$mbw->horsePower=125;
$mbw->drive();
$diau = new Car();
$diau->horsePower=200;
$diau->drive();
echo "----------------------------------- <br>";

$vw = new Car2(0);
$vw->drive();
$mbw = new Car2(125);
$mbw->drive();
$diau = new Car2(250);
$diau->drive();

echo "----------------------------------- <br>";

$oliver = new Supercar();
$oliver->drive();

echo "<br>----------------------------------- <br>";
