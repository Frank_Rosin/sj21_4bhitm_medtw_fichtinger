<?php
include("PostModel.php");
class PostsRepository
{
    private $db;
    private $password=0;
    private $username=0;
    private $id;
    public function __construct(string $username , string $password) {
        $this->db = new PDO("mysql:host = localhost; dbname = superblog",$this->username = $username, $this->password =$password);
    }

    public function fetchpost(int $id){
        $stmt = $this->db->prepare("SELECT * FROM posts WHERE id=:id");
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();

        $info = $stmt->fetch();
        $post = new PostModel($info["id"], $info["title"],$info["content"]);

        return $post;
    }

    public function fetchposts(int $id){
        $stmt = $this->db->prepare("SELECT * FROM posts WHERE id=:id");
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();

        $info = $stmt->fetch();
        $post = new PostModel($info["id"], $info["title"],$info["content"]);

        return $array = [$post];
    }
}