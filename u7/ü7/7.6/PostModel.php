<?php

class PostModel
{
   public $id;
   public $title;
   public $content;

    public function __construct($id,$title,$content) {
       $this->title = $title;
       $this->content =$content;
       $this->id=$id;
    }
}