<?php

class Car2
{
    private $horsePower;
    function __construct($horsePower = 0) {
        $this->horsePower = $horsePower;
    }

    public function setHorsePower(int $horsePower){
        $this -> horsePower = $horsePower;
    }
    public function getHorsePower(): int{
        return $this -> $horsePower;
    }

    public function  drive(){
        echo "Das Auto hat ".$this->horsePower."PS <br>";
    }

}

?>