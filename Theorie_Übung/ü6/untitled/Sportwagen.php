<?php

class Sportwagen extends Auto
{

    public $geschwindigkeit;
    private $hoechstgeschwindigkeit = 250;

    public function __construct($geschwindigkeit)
    {
        $this->geschwindigkeit = $geschwindigkeit;
    }

    public function bremsen($change)
    {
        echo "Sportwagen: ";
        parent::bremsen($change);
    }
    public function beschleunigen($change)
    {
        if(($this->geschwindigkeit + $change) <= $this->hoechstgeschwindigkeit)
        {
            echo "Sportwagen: ";
            parent::beschleunigen($change);
        }
        else{
            echo "Zu schnell";
            echo "<br>";
        }
    }

}