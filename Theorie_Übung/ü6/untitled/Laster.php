<?php

class Laster extends Auto
{
    public $geschwindigkeit;
    private $hoechstgeschwindigkeit = 120;

    public function __construct($geschwindigkeit)
    {
        $this->geschwindigkeit = $geschwindigkeit;
    }

    public function bremsen($change)
    {
        echo "Laster: ";
        parent::bremsen($change);
    }
    public function beschleunigen($change)
    {
        if(($this->geschwindigkeit + $change) <= $this->hoechstgeschwindigkeit)
        {
            echo "Laster: ";
            parent::beschleunigen($change);
        }
        else{
            echo "Zu schnell";
            echo "<br>";
        }
    }
}