<?php
require_once("Konto.class.php");
class Kunde
{
  protected $vorname;
  protected $nachname = "";
  protected $alter = 0;
  protected $geschlecht = "";
  protected $adresse = "";
  protected $konto = null;

  public function __construct(Konto $konto) {

    $this -> konto = $konto;



  }

  public function getvorname() {
    return $this->vorname;
  }

  public function setvorname($vorname) {
    $this->vorname = $vorname;
  }

  public function getnachname() {
    return $this->nachname;
  }

  public function setnachname($nachname) {
    $this->password = $nachname;
  }

  public function getalter() {
    return $this->alter;
  }

  public function setalter($alter) {
    $this->alter = $alter;
  }

  public function getgeschlecht() {
    return $this->geschlecht;
  }

  public function setgeschlecht($geschlecht) {
    $this->geschlecht = $geschlecht;
  }

  public function getadresse() {
    return $this->adresse;
  }

  public function setadresse($adresse) {
    $this->adresse = $adresse;
  }

  public function getkonto() {
    return $this->konto;
  }


  public function __destruct() {
		echo "<br />Objekt mit dem Namen " . $this -> vorname . " " . $this -> nachname ." wird gel&ouml;scht...";
  }

}
?>
