<?php
require_once("Auto.php");
require_once("Sportwagen.php");
require_once("Laster.php");
class Program
{
    public function __construct(){
        $this -> main();
    }

    private function main(){
        $auto = new Auto(42);
        $auto->beschleunigen(4);
        $auto->beschleunigen(6);
        $auto ->bremsen(100);
        $auto ->bremsen(30);
        $auto->beschleunigen(359);

        $sport = new Sportwagen(100);
        $sport->beschleunigen(12);
        $sport->bremsen(46);

        $laster = new Laster(50);
        $laster->beschleunigen(12);
        $laster->bremsen(22);
    }
}
new Program();

