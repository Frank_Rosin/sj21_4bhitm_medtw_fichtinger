<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PHP</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<?php
$pdo = new PDO('mysql:host=localhost;dbname=contactform', "htl", "insy");

$fnErr = $lnErr = $emailErr = $genderErr = "";
$gender = $title = $first = $last = $email = $street = $plz = $location = $inputbundesland = $field = "";
if (isset($_POST["submit"])) {
    $gender = "";
    $title = $_POST["inputTitle"];
    $first = $_POST["inputFirstName"];
    $last = $_POST["inputLastName"];
    $email = $_POST["inputEmail"];
    $street = $_POST["inputstreet"];
    $plz = $_POST["inputplz"];
    $location = $_POST["inputlocation"];
    $inputbundesland = "";
    $s = "";
    $sy = "";
    $m = "";
    $cnt = 0;
    if (empty($_POST['inputFirstName'])) {
        $fnErr = "First name is required!";
    } else {
        $cnt++;
    }
    if (empty($_POST['inputLastName'])) {
        $lnErr = "Last name is required!";
    } else {
        $cnt++;
    }
    if (empty($_POST['inputEmail'])) {
        $emailErr = "Email is required!";
    } else {
        $cnt++;
    }
    if (!isset($_POST['gender'])) {
        $genderErr = "Gender is required!";
    } else {
        $cnt++;
    }

    if (isset($_POST['gender'])) {
        $gender = $_POST['gender'];
    }

    if (isset($_POST['inputbundesland'])) {
        $inputbundesland = $_POST['inputbundesland'];
    }
    if(!empty($_POST['fieldlist'])){
       $field = $_POST['fieldlist'];

    }





    $null = "";
    $stmt = $pdo->prepare("INSERT INTO users(FIRST_NAME, LAST_NAME, EMAIL, GENDER) VALUES(:first, :last, :email, :gender)");
    $stmt->bindParam(':first', $first);
    $stmt->bindParam(':last', $last);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':gender', $gender);


    $stmt->execute();

    $user_id = $pdo->lastInsertId();


    $statement = $pdo->prepare("UPDATE users SET STREET = :street, CITY = :city, POSTCODE = :postcode, STATE = :state WHERE USER_ID = $user_id ");
    if (isset($street)) {
        $statement->bindParam(':street', $street);
    } else {
        $statement->bindParam(':street', $null);
    }
    if (isset($location)) {
        $statement->bindParam(':city', $location);
    } else {
        $statement->bindParam(':city', $null);
    }
    if (isset($plz)) {
        $statement->bindParam(':postcode', $plz);
    } else {
        $statement->bindParam(':postcode', $null);
    }
    if (isset($inputbundesland)) {
        $statement->bindParam(':state', $inputbundesland);
    } else {
        $statement->bindParam(':state', $null);
    }

    $statement->execute();

  if(!empty($field)){
      foreach ($field as $item){
    $st = $pdo->prepare("INSERT INTO user_has_fields_jt(USER_ID,FIELD) VALUES(:id , :field)");
    $st->bindParam(':id', $user_id);
    $st->bindParam(':field', $item);
    $st->execute();
      }
  }

}
    ?>





<h1>Kontaktformular</h1>
<div class="container-fluid <?php if($cnt !=4) echo "b" ?>">
    <?php echo "Title:" .$title . '<br>', "First Name:" .$first . '<br>', "Last Name:" .$last . '<br>',"Email:" . $email. '<br>', "Location:" .$location. '<br>', "PLZ:" . $plz. '<br>', "GENDER:" .$gender. '<br>', "STREET:" .$street. '<br>'?>
    <?php
    if(!empty($field)) {
        echo "SUBJECTS:";
        foreach ($field as $item) {
            echo $item." ";
        }
    } ?>
    <?echo 'The user id ' . $user_id . ' was inserted';?>
    <h2>Thanks for your feedback!</h2>
    <a href="second.php">Look at database values</a>

</div>
    <div class="container-fluid <?php if($cnt ==4) echo "b" ?>">


    <form method="post" class="bg-dark text-white">

        <p>Select your Gender:</p>
        <fieldset>
            <input type="radio" id="male" name="gender" value="Male" <?php if($gender == "Male") echo 'checked'; ?>>
            <label for="html">male</label><br>
            <input type="radio" id="female" name="gender" value="Female" <?php if($gender == "Female") echo 'checked'; ?>>
            <label for="css">female</label><br>
            <input type="radio" id="other" name="gender" value="Other" <?php if($gender == "Other") echo 'checked'; ?>>
            <label for="javascript">other</label>
        </fieldset>
        <?php echo '<p style="color: red">' . $genderErr . '</p>' ?>
        <div class="row">
            <div class="col-2">
        <div class="form-group">
            <label for="exampleInputPassword1">Title:</label>
            <input type="text" class="form-control  " name="inputTitle" value="<?php
            echo $title;
            ?>" id="inputTitle"
                   placeholder="Title" value="">
        </div>
            </div>
            <div class="col">
        <div class="form-group">
            <label for="exampleInputPassword1">First Name:</label>
            <input type="text" class="form-control " name="inputFirstName" id="inputFirstName" value="<?php
                echo $first;
             ?>"
                   placeholder="First Name"> <?php echo '<p style="color: red">' . $fnErr . '</p>' ?>
        </div>
            </div>
            <div class="col">
        <div class="form-group">
            <label for="exampleInputPassword1">Last Name:</label>
            <input type="text" class="form-control " name="inputLastName" id="inputLastName" value="<?php
            echo $last;
            ?>"
                   placeholder="Last Name"> <?php echo '<p style="color: red">' . $lnErr . '</p>' ?>
        </div>
            </div>
            <div class="col-4">
        <div class="form-group">
            <label for="exampleInputPassword1">Email address</label>
            <input type="email" class="form-control" name="inputEmail" id="inputEmail" aria-describedby="emailHelp" value="<?php
            echo $email;
            ?>"
                   placeholder="Email"> <?php echo '<p style="color: red">' . $emailErr . '</p>' ?>
        </div>
        </div>
        </div>
        <div class="row">
            <div class="col-4">
        <div class="form-group">
            <label for="exampleInputPassword1">Street</label>
            <input type="text" class="form-control" name="inputstreet" id="inputstreet" value="<?php
            echo $street;
            ?>"
                   aria-describedby="emailHelp" placeholder="Street">
        </div>
            </div>
            <div class="col">
        <div class="form-group">
            <label for="exampleInputPassword1">PLZ</label>
            <input type="text" class="form-control" name="inputplz" id="inputplz" aria-describedby="emailHelp" value="<?php
            echo $plz;
            ?>"
                   placeholder="PLZ">
        </div>
            </div>
            <div class="col">
        <div class="form-group">
            <label for="exampleInputPassword1">Location</label>
            <input type="text" class="form-control " name="inputlocation" id="inputlocation" value="<?php
            echo $location;
            ?>"
                   aria-describedby="emailHelp" placeholder="Location">
        </div>
            </div>
            <div class="col-2">
        <div class="form-group">
            <label for="exampleFormControlSelect1">State</label>
            <select class="form-control" name="inputbundesland" id="exampleFormControlSelect1">
                <option <?php if($inputbundesland == "Lower Austria") echo 'selected'; ?>>Lower Austria</option>
                <option <?php if($inputbundesland == "Vienna") echo 'selected'; ?>>Vienna</option>
                <option <?php if($inputbundesland == "Vorarlberg") echo 'selected'; ?>>Vorarlberg</option>
                <option <?php if($inputbundesland == "Tyrol") echo 'selected'; ?>>Tyrol</option>
                <option <?php if($inputbundesland == "Salzburg") echo 'selected'; ?>>Salzburg</option>
                <option <?php if($inputbundesland == "Upper Austria") echo 'selected'; ?>>Upper Austria</option>
                <option <?php if($inputbundesland == "Styria") echo 'selected'; ?>>Styria</option>
                <option <?php if($inputbundesland == "Carinthia") echo 'selected'; ?>>Carinthia</option>
                <option <?php if($inputbundesland == "Burgenland") echo 'selected'; ?>>Burgenland</option>
            </select>
        </div>
            </div>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="SEW" name="fieldlist[]" id="SEW" >
            <label class="form-check-label" for="defaultCheck1">
                SEW
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="MEDT" name="fieldlist[]" id="MEDT" >
            <label class="form-check-label" for="defaultCheck1">
                MEDT
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="SYT" name="fieldlist[]" id="SYT">
            <label class="form-check-label" for="defaultCheck1">
                SYT
            </label>
        </div>
        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
    </form>


        <form method="get">
            <h1>Want to send a Email?</h1>
            <h4>Recipient</h4>
            <input type="text" name="a">
            <button type="link" href="mailer.php?a=<?php echo $_GET['a'] ?>" name="link" class="btn btn-primary"><a href="mailer.php?a=<?php echo $_GET['a'] ?>" style="color:white"> Send </a></button>
        </form>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
</body>
</html>

