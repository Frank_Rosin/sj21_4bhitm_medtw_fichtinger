<?php
$user = "htl";
$pass = "insy";

try {
    $pdo = new PDO('mysql:host=localhost;dbname=trackstar', $user, $pass);
} catch (PDOException $e) {
    die('Error: ' . $e->getMessage() . ' Code: ' . $e->getCode());
}
?>
<!doctype html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Option 1: Include in HTML -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <link rel="stylesheet" href="css/styles.css">

    <title>Trackstar</title>

</head>
<body style="background-color: #353C45">

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Trackstar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>

<?php if (isset($_GET['modifyid'])) {
    $idmodify = $_GET["modifyid"];
    $title = "";
    $desc = "";


    echo "  <form method='post' class='bg-dark rounded h-25 center text-center w-50 d-flex justify-content-center '>";
    echo "  <input type = 'text' id = 'title' name = 'title' placeholder= 'Title' ><br >";
    echo "  <input type = 'text' id = 'desc' name = 'desc' placeholder= 'Description' ><br ><br >";
    echo" <button type='submit' name='submit' class='btn btn-primary'>Submit</button>";
    echo "</form >";
    print($title);
    if (isset($_POST["submit"])) {
        $title = $_POST['title'];
        $desc = $_POST['desc'];
        $SQL = $pdo->prepare("UPDATE projects SET title = :title, description = :desc  WHERE project_id = $idmodify ");
        $SQL -> bindParam(':title', $title);
        $SQL -> bindParam(':desc', $desc);
        $SQL -> execute();

    }
} ?>

<?php $stmt = $pdo->query("SELECT * FROM projects"); ?>
<h2 class="headline">Projects</h2>
<table class="table table-dark table-striped mt-3">
    <thead class="thead">
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Description</th>
        <th scope="col">Date</th>
        <th scope="col">Operations</th>
    </tr>
    </thead>
    <tbody>
    <?php
    // output data of each row
    while ($row = $stmt->fetch()) {
    ?>

    <tr>
        <td><?php echo $row['title']; ?></td>
        <td><?php echo $row['description']; ?></td>
        <td><?php echo $row['created_at']; ?></td>
        <td>
            <?php $delete = $row['project_id'];
            $idissue = $row['project_id'];
            $modify = $row['project_id'];
            echo "<a href='index.php?del=$delete' class='btn bi bi-trash' style='color: white'></a>";

            echo " <a href='index.php?modifyid=$modify' class='btn bi bi-pencil' style='color: white'></a>";

            echo " <a href='index.php?rowid=$idissue' class='btn bi bi-card-list' style='color: white'></a>";
            echo "  </td>";
            echo " </tr>";
            ?>
            <?php
            }
            ?>
    </tbody>
</table>

<!-- Delete -->
<?php
if (isset($_GET['del'])) {
    $del_stmt = $pdo->query("DELETE FROM projects WHERE project_id = " . $_GET['del']);
    $del_issue_stmt = $pdo->query("DELETE FROM issues WHERE project_id = " . $_GET['del']);

    echo '<div class="alert alert-success alert-size" role="alert">';
    echo 'Project successfully deleted: ' . $_GET['del'];
    echo '</div>';
}
?>

<!-- Issues -->
<?php if (isset($_GET['rowid'])) {
    $n = $_GET['rowid'];
    $issue_stmt = $pdo->query("SELECT * FROM issues WHERE project_id = $n");
    if ($issue_stmt->rowCount() > 0) {
        ?>
        <h2 class="headline headline-issues">Issues</h2>
        <table class="table table-dark table-striped mt-3">
            <thead class="thead">
            <tr>
                <th scope="col">Project ID</th>
                <th scope="col">Category</th>
                <th scope="col">Description</th>
                <th scope="col">Created at</th>
            </tr>
            </thead>
            <tbody>
            <?php
            // output data of each row
            while ($row = $issue_stmt->fetch()) {
                ?>
                <tr>
                    <td><?php echo $row['project_id']; ?></td>
                    <td><?php echo $row['category']; ?></td>
                    <td><?php echo $row['description']; ?></td>
                    <td><?php echo $row['created_at']; ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <?php
    }
    else{
        echo '<div class="alert alert-danger" role="alert"> No Issues </div>';
    }

    echo "<a type='button' href='index.php?add=$n' name='addiss' class='btn btn-success btn-issue'>Add Issue</a>";
}

if(isset($_GET["add"])){
    echo "  <form method='post' class='bg-dark text-white rounded w-50 center '>";
    echo "   <label for='title' > Category:</label ><br >";
    echo "  <input type = 'text' id = 'title' name = 't'  ><br >";
    echo "   <label for='desc' > Description:</label ><br >";
    echo "  <input type = 'text' id = 'desc' name = 'd'  ><br ><br >";
    echo" <button type='submit' name='addsubmit' class='btn btn-primary'>Submit</button>";
    echo "</form>";
    $t = "";
    $d = "";
    $addid = $_GET["add"];
    if(isset($_POST["addsubmit"])) {
        $t = $_POST["t"];
        $d = $_POST["d"];

        $c =date("Y/m/d");
        $stmt = $pdo->prepare("INSERT INTO issues(category, description, created_at, project_id) VALUES(:cat, :des, :crea, :add)");
        $stmt->bindParam(':cat', $t);
        $stmt->bindParam(':des', $d);
        $stmt->bindParam(':crea', $c);
        $stmt->bindParam(':add', $addid);
        $stmt->execute();

    }
}


?>



<footer class="bg-dark text-center text-white mt-5 foot">
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © 2022 Copyright:
        <a class="text-white" href="https://mdbootstrap.com/">Nico Fichtinger</a>
    </div>
</footer>

</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</html>
