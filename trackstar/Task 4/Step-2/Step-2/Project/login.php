<?php
$user = "htl";
$pass = "insy";

try {
    $pdo = new PDO('mysql:host=localhost;dbname=trackstar', $user, $pass);
} catch (PDOException $e) {
    die('Error: ' . $e->getMessage() . ' Code: ' . $e->getCode());
}
?>
<!doctype html>
<html lang="en">
<head>
    <title>Login 08</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="css/style.css">

</head>
<body>
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 text-center mb-5">
                <h2 class="heading-section">Trackstar</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-5">
                <div class="login-wrap p-4 p-md-5">
                    <div class="icon d-flex align-items-center justify-content-center">
                        <span class="fa fa-user-o"></span>
                    </div>
                    <h3 class="text-center mb-4">Login</h3>
                    <form method='post' class="login-form">
                        <div class="form-group">
                            <input type="text" class="form-control rounded-left" placeholder="Username" name="User"
                                   required>
                        </div>
                        <div class="form-group d-flex">
                            <input type="password" class="form-control rounded-left" placeholder="Password" name="Pass"
                                   required>
                        </div>
                        <div class="form-group d-md-flex">
                            <div class="w-50">
                                <label class="checkbox-wrap checkbox-primary">Remember Me
                                    <input type="checkbox" checked>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="w-50 text-md-right">
                                <a href="register.php">Register</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php
                    $pass = "";
                    $user = "";
                    if(isset($_POST["Pass"])){
                        $pass = $_POST["Pass"];
                    }
                    if(isset($_POST["User"])){
                        $user = $_POST["User"];
                    }
                            echo " <button type='submit'  class='btn btn-primary rounded submit p-3 px-5'>Login</button>";
                    session_start();
                    $_SESSION['username'] = $user;

                    $stmt = $pdo->query("SELECT * FROM users");
                    while ($row = $stmt->fetch()){


                          if (password_verify($pass, $row["password"])) {
                                  if ($user == $row["username"]) {
                                      header("Location: index.php");
                                  }
                          }


                    }

                    ?>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="js/jquery.min.js"></script>
<script src="js/popper.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>

