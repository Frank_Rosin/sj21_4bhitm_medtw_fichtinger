<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php
try {
    $pdo = new PDO('mysql:host=localhost;dbname=trackstar', "htl", "insy");
} catch (PDOException $e) {
    die('Error: ' . $e->getMessage() . ' Code: ' . $e->getCode());
}
?>
<nav class="navbar sticky-top navbar-expand-lg navbar-light bg-white navim">
    <a class="navbar-brand" href="#">Trackstar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#"> <i class="bi bi-person-fill"></i>
                    <span class="sr-only">(current)</span></a>
            </li>
    </div>
</nav>
<div class="row">
    <div class="col">
        <h1 id="ueber">Projekt Übersicht</h1>
    </div>
</div>
<?php
$stmt = $pdo->query("SELECT * FROM projects");
$result = $stmt->fetchAll();
echo "<div class='cont'>";
echo "<table class='table  table-dark table-borderless align-content-end'>";
echo " <thead>";
echo " <tr>";
echo "  <th class='w-25' scope='col'>Name</th>";
echo "  <th class='w-25'scope='col'>Description</th>";
echo "  <th class='w-25'scope='col'>Date</th>";
echo "  <th class='w-25'scope='col'>Operations</th>";
echo " </tr>";
echo "</thead>";
foreach ($result as $item) {
    echo "<table class='table table-dark table-borderless table-striped table-hover '>";
    echo "<tbody>";
    echo "<tr>";
    echo " <td class='w-25'>" . $item["title"] . " " . "</td>";
    echo " <td class='w-25'>" . $item["description"] . " " . "</td>";
    echo " <td class='w-25'>" . $item["created_at"] . " " . "</td>";
    echo " <td class='w-25'> <i class='bi bi-trash marg'> </i> <i class='bi bi-pencil marg'></i> <i class='bi bi-list-ul marg'> </td>";
    echo " </tr>";
    echo "</tbody>";
    echo "</table>";

}
echo"</div>";
?>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
<script src="js/js.js"></script>
</body>
</html>
